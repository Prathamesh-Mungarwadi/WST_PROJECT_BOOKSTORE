<?php
  $title = "Contact";
  require_once "./template/header.php";
?>
    <div class="row">
        <div class="col-md-3"></div>
		<div class="col-md-6 text-center">
			<form class="form-horizontal" method="post" action="add_customer.php">
			  	<fieldset>
				    <p class="lead">Create new account</p>
				    <div class="form-group">
				      	<label for="inputName" class="col-lg-2 control-label">Name</label>
				      	<div class="col-lg-10">
				        	<input type="text" class="form-control" name="inputName" placeholder="Name">
				      	</div>
				    </div>
				    <div class="form-group">
				      	<label for="inputEmail" class="col-lg-2 control-label">Email</label>
				      	<div class="col-lg-10">
				        	<input type="text" name="inputEmail" class="form-control" id="inputEmail" placeholder="Email">
				      	</div>
				    </div>
				    <div class="form-group">
				      	<label for="password" class="col-lg-2 control-label">Password</label>
				        <input type="password" name="pass">
				    </div>
				    <div class="form-group">
				      	<div class="col-lg-10 col-lg-offset-2">
						<button type="Sign_up" name="Sign_up" class="btn btn-primary">Sign up</button>
				        	<button type="reset" class="btn btn-default">Cancel</button>
				      	</div>
				    </div>
			  	</fieldset>
			</form>
		</div>
		<div class="col-md-3"></div>
    </div>
<?php
  require_once "./template/footer.php";
?>
